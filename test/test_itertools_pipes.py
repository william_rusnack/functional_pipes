import unittest, io
import itertools as it

from functional_pipes import Pipe
Pipe.load('built_in_functions', 'itertools_pipes')


class TestMethods(unittest.TestCase):
  @classmethod
  def setUpClass(self):
    Pipe.load('built_in_functions', 'itertools_pipes')

  @classmethod
  def tearDownClass(self):
    Pipe.unload('built_in_functions', 'itertools_pipes')

# group_by is not passing yet
  # def test_groupby_no_key(self):
  #   data = 1, 2, 3, 1, 2, 3, 4

  #   to_tuple = lambda groups: tuple((key, tuple(group)) for key, group in groups)

  #   self.assertEqual(
  #       to_tuple(Pipe(data).groupby()),
  #       to_tuple(it.groupby(data))
  #     )

  # def test_groupby_with_key(self):
  #   data = 1, 2, 3, 1, 2, 3, 4

  #   key = lambda val: val % 2

  #   to_tuple = lambda groups: tuple((key, tuple(group)) for key, group in groups)

  #   ref = to_tuple(it.groupby(data, key))

  #   self.assertEqual(
  #       to_tuple(Pipe(data).groupby(key)),
  #       ref
  #     )

  #   pipe_reuse = Pipe().groupby(key)

  #   self.assertEqual(
  #       to_tuple(pipe_reuse(data)),
  #       ref
  #     )
  #   self.assertEqual(
  #       to_tuple(pipe_reuse(data)),
  #       ref
  #     )  # reload pipe

  # def test_groupby_key(self):
  #   data = (1, 2), (3, 4), (1, 5)

  #   key = lambda key_val: key_val[0]

  #   to_tuple = lambda groups: tuple((key, tuple(group)) for key, group in groups)

  #   ref = to_tuple(it.groupby(data, key))

  #   self.assertEqual(
  #       to_tuple(Pipe(data).groupby_key()),
  #       ref
  #     )

  #   pipe_reuse = Pipe().groupby_key()

  #   self.assertEqual(
  #       to_tuple(pipe_reuse(data)),
  #       ref
  #     )
  #   self.assertEqual(
  #       to_tuple(pipe_reuse(data)),
  #       ref
  #     )  # reload pipe

  def test_permutations(self):
    data = 1, 2, 3
    ref_all = tuple(it.permutations(data))
    ref_two = tuple(it.permutations(data, 2))

    self.assertEqual(Pipe(data).permutations().tuple(), ref_all)
    self.assertEqual(Pipe(data).permutations(2).tuple(), ref_two)

    pipe_all = Pipe().permutations().tuple()
    self.assertEqual(pipe_all(data), ref_all)
    self.assertEqual(pipe_all(data), ref_all) # not a repeat

    pipe_two = Pipe().permutations(2).tuple()
    self.assertEqual(pipe_two(data), ref_two)
    self.assertEqual(pipe_two(data), ref_two) # not a repeat

  def test_product(self):
    data = 0, -1
    repeat = 2
    ref = tuple(it.product(data, repeat=repeat))

    self.assertEqual(Pipe(data).product(repeat=repeat).tuple(), ref)

    pipe_product = Pipe().product(repeat=repeat).tuple()
    self.assertEqual(pipe_product(data), ref)
    self.assertEqual(pipe_product(data), ref) # not a repeat


if __name__ == '__main__':
  unittest.main()

