'''
methods that come from python's itertools package

groupby is not working yet
'''

import itertools as it

from functional_pipes.wrap_gener import wrap_gener


# def groupby_key(iterable):
#   return it.groupby(iterable, lambda key_val: key_val[0])


# methods
methods_to_add = (
    # it.groupby,
    # groupby_key,
    wrap_gener(it.permutations),
    wrap_gener(it.product),
  )


# map methods
map_methods_to_add = (
  )
